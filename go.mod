module gitlab.com/notera-proj/graphql-server

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
)
